package model3;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorFrame3 extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel colorPanel;
	private JCheckBox redCheckBox;
	private JCheckBox greenCheckBox;
	private JCheckBox blueCheckBox;

	private static final int WIDTH = 300;
	private static final int HEIGHT = 400;

	public ColorFrame3() {
		colorPanel = new JPanel();

		add(colorPanel ,BorderLayout.CENTER);
		createControlPanel();
		setSampleColor();
		setSize(WIDTH,HEIGHT);
	}

	public void createControlPanel(){
		class ColorListener implements ChangeListener
		{
			@Override
			public void stateChanged(ChangeEvent arg0) {
				setSampleColor();

			}
		}

		ChangeListener listener = new ColorListener();

		redCheckBox = new JCheckBox("Red");
		redCheckBox.addChangeListener(listener);

		greenCheckBox = new JCheckBox("Green");
		greenCheckBox.addChangeListener(listener);

		blueCheckBox = new JCheckBox("Blue");
		blueCheckBox.addChangeListener(listener);

		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new GridLayout(3, 2));

		controlPanel.add(redCheckBox);
		controlPanel.add(greenCheckBox);
		controlPanel.add(blueCheckBox);

		add(controlPanel,BorderLayout.SOUTH);

	}

	public void setSampleColor(){
		
//		Color red = Color.red;
//		Color blue = Color.blue;
//		Color green = Color.green;
		
		if (redCheckBox.isSelected()) {
			colorPanel.setBackground(Color.red);}
		if (redCheckBox.isSelected() && greenCheckBox.isSelected()) {
			colorPanel.setBackground(Color.yellow);}
		if (redCheckBox.isSelected() && blueCheckBox.isSelected()) {
			colorPanel.setBackground(Color.cyan);}
		
		if (greenCheckBox.isSelected()) {
			colorPanel.setBackground(Color.green);}
//		if (greenCheckBox.isSelected() && blueCheckBox.isSelected()) {
//			colorPanel.setBackground(Color.cyan);}
		if (greenCheckBox.isSelected() && blueCheckBox.isSelected()) {
			colorPanel.setBackground(Color.pink);}
		
//		if (blueCheckBox.isSelected()) {
//			colorPanel.setBackground(Color.blue);}	
//		
//		if (redCheckBox.isSelected() && greenCheckBox.isSelected() && blueCheckBox.isSelected()) {
//			colorPanel.setBackground(Color.white);}

		colorPanel.repaint();
	} 
}
