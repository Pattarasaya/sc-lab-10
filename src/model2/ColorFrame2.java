package model2;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorFrame2 extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel colorPanel;
	private JRadioButton redRadioButton;
	private JRadioButton greenRadioButton;
	private JRadioButton blueRadioButton;

	private static final int WIDTH = 300;
	private static final int HEIGHT = 400;

	public ColorFrame2() {
		colorPanel = new JPanel();

		add(colorPanel ,BorderLayout.CENTER);
		createControlPanel();
		setSampleColor();
		setSize(WIDTH,HEIGHT);
	}

	public void createControlPanel(){
		class ColorListener implements ChangeListener
		{
			@Override
			public void stateChanged(ChangeEvent arg0) {
				setSampleColor();

			}
		}

		ChangeListener listener = new ColorListener();

		redRadioButton = new JRadioButton("Red");
		redRadioButton.addChangeListener(listener);

		greenRadioButton = new JRadioButton("Green");
		greenRadioButton.addChangeListener(listener);

		blueRadioButton = new JRadioButton("Blue");
		blueRadioButton.addChangeListener(listener);
		
		ButtonGroup group = new ButtonGroup();
		group.add(redRadioButton);
		group.add(greenRadioButton);
		group.add(blueRadioButton);

		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new GridLayout(3, 2));

		controlPanel.add(redRadioButton);
		controlPanel.add(greenRadioButton);
		controlPanel.add(blueRadioButton);

		add(controlPanel,BorderLayout.SOUTH);
		
	}

	public void setSampleColor(){

	
		if(redRadioButton.isSelected()){
			colorPanel.setBackground(Color.red);
		}else if (greenRadioButton.isSelected()) {
			colorPanel.setBackground(Color.green);	
		}else if (blueRadioButton.isSelected()) {
			colorPanel.setBackground(Color.blue);
		}
		
		colorPanel.repaint();
	}
}
