package model4;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorFrame4 extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel colorPanel;
	private JComboBox colorCombo;
	private ChangeListener listener;

	private static final int WIDTH = 300;
	private static final int HEIGHT = 400;

	public ColorFrame4() {
		
		class ColorListener implements ChangeListener
		{
			@Override
			public void stateChanged(ChangeEvent event) {
				setSampleColor();

			}
		}

		listener = new ColorListener(); 
		
		colorPanel = new JPanel();

		add(colorPanel ,BorderLayout.CENTER);
		createColorPanel();
		setSampleColor();
		setSize(WIDTH,HEIGHT);
	}

	public JPanel createColorPanel(){

		colorCombo = new JComboBox();
		colorCombo.addItem("Red");
		colorCombo.addItem("Green");
		colorCombo.addItem("Blue");
		colorCombo.setEditable(false);
		colorCombo.addActionListener(colorCombo);

		JPanel controlPanel = new JPanel();

		controlPanel.add(colorCombo);

		add(controlPanel,BorderLayout.SOUTH);
		return controlPanel;

	}

	public void setSampleColor(){
		colorCombo.getSelectedItem();
		colorPanel.getBackground();
//		colorPanel.setBackground(Color.red);
		

		colorPanel.repaint();
	} 
}
