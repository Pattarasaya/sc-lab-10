package model1;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ColorFrame1 extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel colorPanel;
	private JSlider redSlider;
	private JSlider greenSlider;
	private JSlider blueSlider;

	private static final int WIDTH = 300;
	private static final int HEIGHT = 400;

	public ColorFrame1() {
		colorPanel = new JPanel();

		add(colorPanel ,BorderLayout.CENTER);
		createControlPanel();
		setSampleColor();
		setSize(WIDTH,HEIGHT);
	}

	public void createControlPanel(){
		class ColorListener implements ChangeListener
		{
			@Override
			public void stateChanged(ChangeEvent arg0) {
				setSampleColor();

			}
		}

		ChangeListener listener = new ColorListener();

		redSlider = new JSlider(0,255,255);
		redSlider.addChangeListener(listener);

		greenSlider = new JSlider(0,255,175);
		greenSlider.addChangeListener(listener);

		blueSlider = new JSlider(0,255,175);
		blueSlider.addChangeListener(listener);

		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new GridLayout(3, 2));

		controlPanel.add(new JLabel("Red"));
		controlPanel.add(redSlider);

		controlPanel.add(new JLabel("Green"));
		controlPanel.add(greenSlider);

		controlPanel.add(new JLabel("Blue"));
		controlPanel.add(blueSlider);

		add(controlPanel,BorderLayout.SOUTH);

	}

	public void setSampleColor(){

		int red = redSlider.getValue();
		int green = greenSlider.getValue();
		int blue = blueSlider.getValue();

		Color backColor = new Color(red,green,blue);
		colorPanel.setBackground(backColor);
		colorPanel.repaint();
	}
}
